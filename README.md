* installare il tema:
    git submodule update --init --recursive
* nota. La commit del tema (cupper) con cui è stato pubblicato tutto questo è la 250f5e4.
* Eseguire il server localmente
     docker run --rm -it \
      -v $(pwd):/src \
      -p 1313:1313 \
      klakegg/hugo:0.78.2 \
      server

