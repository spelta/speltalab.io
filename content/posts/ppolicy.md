---
title: "Pitigram privacy policy"
date: 2024-03-04T09:18:11+01:00
draft: false
---

Pitigram does not access nor treat, manipulate, or distribute any user data whatsoever.

