---
title: San Valentino
date: 2019-02-14
---

Oggi è san Valentino. Non ho preso fiori alla mia futura moglie.

Non so cosa caricherò in futuro in questo sito, però il primo pensiero mi sembrava doveroso dedicarlo a lei.


## Mi piaci

Mi piace quando ci sei e mi piace anche quando non ci sei.
In questo puzzle di pezzi in equilibrio, sei quello più bello; che anche quando si sposta lascia il disegno bello da vedere.

Il pezzo che si rimette sempre, che combacia perfettamente; e che quando ci sembra diversamente è perché non ci stiamo accorgendo che siamo noi che lo stiamo mettendo al contrario.

Ciao, Patola.


